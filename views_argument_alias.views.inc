<?php
/**
 * Implements hook_views_plugins().
 */
function views_argument_alias_views_plugins() {
  return array(
    'argument validator' => array(
      'views_argument_alias' => array(
        'title' => t('Path alias'),
        'handler' => 'views_argument_alias_views_plugin_argument_validate_alias',
        'path' => drupal_get_path('module', 'views_argument_alias') . '/plugins',
        'parent' => 'fixed', // so that the parent class is included
      ),
    ),
  );
}
