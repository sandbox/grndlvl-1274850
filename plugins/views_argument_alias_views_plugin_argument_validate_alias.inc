<?php
/**
 * @file
 * Path alias validator.
 */

/**
 * Path alias validator plugin.
 */
class views_argument_alias_views_plugin_argument_validate_alias extends views_plugin_argument_validate {
  function option_definition() {
    $options = parent::option_definition();
    $options['module'] = array('default' => '_none');
    $options['custom'] = array('default' => '');

    if (module_exists('pathauto')) {
      // This code is modeled after the pathauto_patterns_form() in pathauto.admin.inc.
      $path_settings = module_invoke_all('pathauto', 'settings');
      foreach ($path_settings as $settings) {
        $module = $settings->module;

        $options[$module] = array('default' => 0);
        $options[$module . '_part'] = array('default' => 0);

        // If the module supports a set of specialized patterns, set
        // them up here.
        if (isset($settings->patternitems)) {
          foreach ($settings->patternitems as $itemname => $itemlabel) {
            $variable = 'pathauto_' . $module . '_' . $itemname . '_pattern';
            if (variable_get($variable, '') != '') {
              $options[$module . '_' . $itemname] = array('default' => 0);
              $options[$module . '_' . $itemname . '_part'] = array('default' => 0);
            }
          }
        }
      }
    }

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['module'] = array(
      '#type' => 'select',
      '#title' => t('Pathauto patterns'),
      '#options' => array('_none' => t('None'), 'custom' => 'Custom'),
      '#default_value' => $this->options['module'],
      '#description' => t('Select which Pathauto module that should be used.'),
    );
    $form['custom'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom pattern'),
      '#description' => t('Enter a custom pattern using "%" to specify which
        part of the pattern you wish to replace with the contextual filter value.') . '<br />'
        . t('You may also specify parts of the url with numbers that represent the values of arg(N) to use within your pattern.') . '<br />'
        . t('For example:') . ' <br />'
        . '<blockquote>' .
          t('Alias: "category/subcategory"') . ' => ' . t('Path to view: "categories/category/subcategory/is/awesome"') . '<br />'
        . t('Custom pattern: "%/2"')
        . '</blockquote>',
      '#default_value' => $this->options['custom'],
      '#dependency' => array(
        'edit-options-validate-options-views-argument-alias-module' => array('custom'),
      ),
    );
    if (module_exists('pathauto')) {
      // This code is modeled after the pathauto_patterns_form() in pathauto.admin.inc.
      $path_settings = module_invoke_all('pathauto', 'settings');
      foreach ($path_settings as $settings) {
        $module = $settings->module;
        $patterndescr = $settings->patterndescr;
        $patterndefault = $settings->patterndefault;
        $form['module']['#options'][$module] = $settings->groupheader;

        $variable = 'pathauto_' . $module . '_pattern';
        // Form builder settings.
        $form_settings = array(
          'element' => $module,
          'title' => $patterndescr,
          'module' => $module,
          'variable' => $variable,
          'var_default' => $patterndefault,
        );
        // Get the pathauto options form.
        $this->build_pathauto_pattern_forms($form, $form_settings);

        // If the module supports a set of specialized patterns, set
        // them up here.
        if (isset($settings->patternitems)) {
          foreach ($settings->patternitems as $itemname => $itemlabel) {
            $variable = 'pathauto_' . $module . '_' . $itemname . '_pattern';
            if (variable_get($variable, '') != '') {
              // Form builder settings.
              $form_settings = array(
                'element' => $module . '_' . $itemname,
                'title' => $itemlabel,
                'module' => $module,
                'variable' => $variable,
                'var_default' => '',
              );
              // Get the pathauto options form.
              $this->build_pathauto_pattern_forms($form, $form_settings);
            }
          }
        }
      }
    }
  }

  function validate_argument($argument) {
    $module = $this->options['module'];
    $aliases = array();
    if ($module == 'custom') {
      $parts = explode('/', $this->options['custom']);
      foreach ($parts as $key => $part) {
        if (is_numeric($part)) {
          $parts[$key] = arg($part);
        }
        if ('%' == $part) {
          $parts[$key] = $argument;
        }
      }
      $aliases[] = implode('/', $parts);
    }
    if ($module != '_none' && $module != 'custom') {
      // We don't want the module in the rest of our options.
      unset($this->options['module']);
      $parts = array();
      foreach ($this->options as $key => $value) {
        // Only check the ones we have settings for.
        if (strpos($key, $module) !== FALSE) {
          // Check only items not parts.
          // A part is only used to determine which part of the pattern to replace.
          if (strpos($key, '_part') === FALSE) {
            // Check to see if the item is enabled.
            if ($this->options[$key] == 1) {
              // Form the pathauto variable.
              $variable = 'pathauto_' . $key . '_pattern';
              $pattern = variable_get($variable, '');
              $parts = explode('/', $pattern);
              // Replace the specified part of the pattern with the argument.
              $parts[$this->options[$key . '_part']] = $argument;
              $aliases[] = implode('/', $parts);
            }
          }
        }
      }
    }

    $normal_path = FALSE;
    // No path auto aliases. Just check if there is an alias.
    if (empty($aliases)) {
      $normal_path = drupal_get_normal_path($argument);
    }
    else {
      // Loop through the specified pathauto patterns to find a match.
      // TODO: Might be worth while making these a weightable.
      foreach ($aliases as $alias) {
        if ($normal_path = drupal_get_normal_path($alias)) {
          break;
        }
      }
    }

    if ($normal_path) {
      $parts = explode('/', $normal_path);
      foreach($parts as $part) {
        if (is_numeric($part)) {
          $this->argument->argument = $part;
          return TRUE;
        }
      }
    }

    // Hey! I didn't find anything for that alias.
    return FALSE;
  }

  /**
   * Form helper to build path auto pattern forms.
   */
  function build_pathauto_pattern_forms(&$form, $settings) {
    $form[$settings['element']] = array(
      '#type' => 'checkbox',
      '#title' => $settings['title'],
      '#default_value' => $this->options[$settings['element']],
      '#description' => t('Enable this pattern for comparision.'),
      '#process' => array('form_process_checkbox', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-validate-options-views-argument-alias-module' => array($settings['module']),
      ),
    );

    $parts = explode('/', variable_get($settings['variable'], ''));
    $form[$settings['element'] . '_part'] = array(
      '#type' => 'radios',
      '#title' => t('Pattern part'),
      '#options' => $parts,
      '#default_value' => $this->options[$settings['element'] . '_part'],
      '#description' => t('Select which part of the pattern the contextual filter value will correspond to.'),
      '#process' => array('form_process_radios', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-validate-options-views-argument-alias-module' => array($settings['module']),
        'edit-options-validate-options-views-argument-alias-' . str_replace('_', '-', $settings['element']) => array(1),
      ),
      '#dependency_count' => 2,
    );
  }
}
