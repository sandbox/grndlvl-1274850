-- SUMMARY --

This is a views argument validator plugin that allows path aliases to be passed as contextual filter values.

"What's that?" You say.

Let's say that there is a node with the internal path of "node/1" indicating
that the nid of this node is 1.
This node also has the url alias of "my-super-cool-article".
Let's also say that there is a view with the path of "article/%".
We now visit the view with the path "article/my-super-cool-article".
With this validator plugin with then replace "my-super-cool-article" with "1" and
may be used in conjucntion with the "Content Nid." Contextual filter.

*I will have to look up some various other use cases... the ones I have usually are pretty specific*

-- REQUIREMENTS --

* Views 7.x-3.x

-- RECOMMENDED --

* Pathauto 7.x-1.x

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONTACT --

Current maintainer:
* Jonathan DeLaigle (grndlvl) - http://drupal.org/user/103553
